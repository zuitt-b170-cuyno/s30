const express = require('express')
const mongoose = require('mongoose')

const app = express(), port = 3000

mongoose.connect("mongodb+srv://jhnnycyn:dxtr008X@cluster0.z5eip.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "Pending"
    }
})

const Task = mongoose.model("Task", taskSchema)

// Controller
app.post("/tasks", (req, res) => {
    Task.findOne({ name: req.body.name }, (error, task) => {
        if (task && task.name === req.body.name)
            return res.send("There is a duplicate task")
        else {
            const newTask = new Task({ name: req.body.name })
            newTask.save((error, success) => error ? console.error(error) : res.status(201).send("New Task Created"))
        }
    })
})

/* 

app.get("/tasks", (req, res) => {
    Task.find({}, (error, result) => {
        if (error) 
            return console.log(error)
        else
            return res.status(200).json({data: result})
    })
})

*/

app.get("/tasks", (req, res) => {
    Task.find((error, task) => error ? res.status(401).send(error) : res.status(200).send(task))
})

// S30 Activity

const userSchema = new mongoose.Schema({
    username: String,
    password: String
}) 

const User = new mongoose.model('User', userSchema)

app.post('/signup', (req, res) => {
    User.findOne({ username: req.body.username }, (error, user) => {
        if (user && user.username === req.body.username) 
            return res.send("Username already in use")
        else {
            const newUser = new User({
                username: req.body.username,
                password: req.body.password
            })

            if (newUser.username && newUser.password) 
                newUser.save((error, success) => error ? console.error(error) : res.status(201).send("Successfully Registered"))
            else 
                res.send("Both username and password must be provided.") 
        }
    })
})

app.get("/users", (req, res) => {
    User.find((error, user) => error ? console.log(error) : res.status(201).send(user))
})

app.listen(port, () => console.log(`Server is running at port:${port}`))